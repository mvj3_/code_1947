package com.guanMac.xml_parser;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.guanMac.DOM.Person;

public class SAXForHandler extends DefaultHandler
{
	private static final String TAG = "SAXForHandler";
	private List<Person> persons;
	// 记录前一个标签的名称
	private String preTag;
	// 记录当前Person
	Person person;

	public List<Person> getPersons()
	{
		return persons;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException
	{
		// TODO Auto-generated method stub
		String data = new String(ch, start, length).trim();
		if (!"".equals(data.trim()))
		{

		}
		if ("name".equals(preTag))
		{
			person.setName(data);
		}
		else if ("age".equals(preTag))
		{
			person.setAge(new Short(data));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException
	{
		// TODO Auto-generated method stub
		super.endDocument();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		// TODO Auto-generated method stub
		if ("person".equals(localName))
		{
			persons.add(person);
			person = null;
		}
		preTag = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument() 适合在此事件中触发初始化行为
	 */
	@Override
	public void startDocument() throws SAXException
	{
		// TODO Auto-generated method stub
		persons = new ArrayList<Person>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String,
	 * java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException
	{
		// TODO Auto-generated method stub
		if ("person".equals(localName))
		{
			for (int i = 0; i < attributes.getLength(); i++)
			{
				person = new Person();
				person.setId(Integer.valueOf(attributes.getValue(i)));
			}
		}
		preTag = localName;
	}

}
