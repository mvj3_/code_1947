// 通过类装载期的方式获得文件路径，再以输入流的方式放入解析器。
		InputStream is = MainActivity.class.getClassLoader()
				.getResourceAsStream("guanMac.xml");
		SAXForHandler handler = new SAXForHandler();
		// 通过一个SAXParserFactory来创建具体SaxParser
		// 通过工厂模式，几时当需要使用不同的解析器时，也能通过改变相应的环境变量，而不是改变代码。
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser saxParser = spf.newSAXParser();
		saxParser.parse(is, handler);

		List<Person> persons = handler.getPersons();
		is.close();